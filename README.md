# Setup

### Requirements

- `node.js` >= 14 (not 10)
- `yarn` (1.x)

### Install dependencies

```bash
yarn
```

# Run

Run with

```bash
yarn todo
```

Supported commands:

- `show` – just list all todo comments
- `user` – filter by user. Example: `user flash-gordon`
- `important` – show only important
- `sort` – sort by three possible options: `importance`, `user`, or `date`
- `date` – show comments after entered date. Examples: `date 2018-03-01` or `date 2015`
- `exit`

# Run tests

```bash
yarn test
```

# Prepare for upload

This command will create `build/Lastname_Firstname.zip`

```bash
yarn build
```
