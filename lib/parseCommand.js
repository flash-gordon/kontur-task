const unknown = { type: 'unknown' }

module.exports = line => {
  switch (line) {
    case 'show':
    case 'exit':
    case 'important':
      return { type: line }
    default:
      const [command, option] = line.split(' ', 2)

      if (option === undefined) {
        return unknown
      } else {
        switch (command) {
          case 'user':
            return { type: 'user', user: option.toLowerCase() }
          case 'sort':
            switch (option) {
              case 'importance':
              case 'user':
              case 'date':
                return { type: 'sort', by: option }
              default:
                return unknown
            }
          case 'date':
            if (/^\d{4}(-\d{2}(-\d{2})?)?$/.test(option)) {
              return { type: 'date', after: option }
            } else {
              return unknown
            }
          default:
            return unknown
        }
      }
  }
}
