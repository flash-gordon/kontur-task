
const headerLine = {
  importance: true,
  date: 'date',
  user: 'user',
  comment: 'comment',
  fileName: 'fileName'
}

const columns = ['importance', 'user', 'date', 'comment', 'fileName']

const headerSizes = columns.slice(1).reduce(
  (acc, c) => ({ ...acc, [c]: headerLine[c].length }),
  { importance: 1 }
)

const mbLength = mbString => mbString === null ? 0 : mbString.length

const sizes = lines => lines.reduce((acc, line) => ({
  importance: acc.importance,
  user: Math.min(Math.max(acc.user, mbLength(line.user)), 10),
  date: Math.min(Math.max(acc.date, mbLength(line.date)), 10),
  comment: Math.min(Math.max(acc.comment, line.comment.length), 50),
  fileName: Math.min(Math.max(acc.fileName, line.fileName.length), 15)
}), headerSizes)

const makeRow = (columns) => `  ${columns.join('  |  ')}  `

const showValue = (column, size, value) => {
  if (column === 'importance') {
    return value ? '!' : ' '
  } else {
    const string = value === null ? '' : value

    if (string.length <= size) {
      return string.padEnd(size, ' ')
    } else {
      return `${string.substring(0, size - 3)}...`
    }
  }
}

module.exports = (lines) => {
  const columnSizes = sizes(lines);
  const columnsWithSizes = columns.map((c, idx) => [c, columnSizes[c], idx])

  const [header, ...body] = [headerLine, ...lines].map(
    l => makeRow(columnsWithSizes.map(([c, s]) => showValue(c, s, l[c])))
  )

  const filler = ''.padEnd(header.length, '-')

  if (body.length === 0) {
    return [header, filler].join("\n")
  } else {
    return [header, filler, ...body, filler].join("\n")
  }
}
