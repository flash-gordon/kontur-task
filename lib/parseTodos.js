const { basename } = require('path')

const makeTodo = (comment, user = null, date = null) => {
  return {
    user,
    date,
    comment,
    importance: comment.length - comment.replace(/!/g, '').length
  }
}

const parseLine = line => {
  const prefixMatch = line.match(/.*\/\/\s+todo\s+/i)

  if (prefixMatch === null) {
    return null
  } else {
    const tail = line.substring(prefixMatch[0].length)

    const formatMatch = tail.split(/; ?/, 3)

    if (formatMatch.length === 3) {
      const [user, date, comment] = formatMatch;

      return makeTodo(comment, user, date)
    } else {
      return makeTodo(tail)
    }
  }
}

const parseLines = content => content.split("\n")

module.exports = (files) => files.flatMap(
  ({ path, content }) => parseLines(content).map(l => {
    const toDo = parseLine(l)

    if (toDo === null) {
      return null
    } else {
      return { fileName: basename(path), ...toDo }
    }
  }).filter(l => l !== null)
);
