const parseTodos = require('./parseTodos')
const showTable = require('./showTable')

const sort = (list, f) => [...list].sort(f)

const compareByKey = (key, compareValues) => (a, b) => {
  if (a[key] === b[key]) {
    return 0
  } else if (b[key] === null) {
    return -1
  } else if (a[key] === null || compareValues(a[key], b[key])) {
    return 1
  } else {
    return -1
  }
}

const compareStrings = (a, b) => {
  if (a < b) {
    return 1
  } else if (b > a) {
    return -1
  } else {
    return 0
  }
}

const sortingFunction = by => {
  switch (by) {
    case 'importance':
      return (a, b) => b.importance - a.importance
    case 'user':
    case 'date':
      return compareByKey(by, compareStrings)
  }
}

const applyCommand = (command, lines) => {
  switch (command.type) {
    case 'show':
      return lines
    case 'important':
      return lines.filter(({ importance }) => importance > 0)
    case 'user':
      return lines.filter(({ user }) => user !== null && user.toLowerCase() === command.user)
    case 'date':
      return lines.filter(({ date }) => date !== null && date >= command.after)
    case 'sort':
      return sort(lines, sortingFunction(command.by))
  }
}

module.exports = (command, files, { print, exit }) => {
  switch (command.type) {
    case 'exit':
      exit()
      break
    case 'unknown':
      print('wrong command')
      break
    default:
      const lines = parseTodos(files)
      print(showTable(applyCommand(command, lines)))
  }
}
