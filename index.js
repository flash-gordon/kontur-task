const { getAllFilePathsWithExtension, readFile } = require('./lib/fileSystem')
const { readLine } = require('./lib/console')
const parseCommand = require('./lib/parseCommand')
const runCommand = require('./lib/runCommand')

app()

function app() {
  const dir = process.argv[2] || process.cwd()
  const files = getFiles(dir)
  const effects = {
    print: l => console.log(l),
    exit: () => process.exit(0)
  }

  effects.print('Please, write your command!')
  readLine(line => runCommand(parseCommand(line), files, effects))
}

function getFiles(dir) {
  const filePaths = getAllFilePathsWithExtension(dir, 'js')
  return filePaths.map(path => ({ path, content: readFile(path) }))
}
